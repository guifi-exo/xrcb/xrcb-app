import React, { useRef, useState } from 'react';

import { View, TextInput, TouchableOpacity } from "react-native";
import Icon from 'react-native-ionicons';
import { DEFAULT_MARGIN } from '../constants';
import { t } from "../services/i18n";

const iconStyles = {
    flex: 0.1,
    marginTop : 5
}

const containerStyles = {
    height: 40,
    flexDirection: "row",
    marginTop: 0,
    paddingTop: 0,
    paddingLeft: DEFAULT_MARGIN / 2,
    paddingRight: DEFAULT_MARGIN / 2,
    borderBottomWidth: 0.5,
    marginBottom: DEFAULT_MARGIN /2,
}

const SearchInput = ({ onChange } : { onChange : (term : string | undefined) => void}) => {
    const inputRef = useRef<TextInput>(null);
    const [showReset, setShowReset] =  useState(false);
    const onChangeInner = (term : string | undefined)  => {
        if(term && term.length > 0) {
            setShowReset(true);
        } else {
            setShowReset(false);
        }
        onChange(term);
    }
    const onReset= ()=> {
        onChangeInner(undefined);
        inputRef.current?.clear();
    }

    const inputStyles = {
        padding: 0,
        flex: showReset ? 0.8 : 0.9,
        margin: 0
    }

    return (
        <View style={containerStyles}>
            <Icon name="search" style={iconStyles} />
            <TextInput style={inputStyles} onChangeText={onChangeInner} placeholder={t("defaultSearch")} ref={inputRef}></TextInput>
            { showReset &&
                <TouchableOpacity onPress={onReset} activeOpacity={1} style={iconStyles}>
                    <Icon name="close" />
                </TouchableOpacity>
            }
        </View>
    )
}
export default SearchInput