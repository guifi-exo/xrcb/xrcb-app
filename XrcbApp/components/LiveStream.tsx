import React, { useState } from "react";
import {  Dimensions, useColorScheme,  View, Text, TouchableOpacity, ImageBackground, Image } from "react-native";
import PlayButton from "./PlayButton";
import PauseButton from "./PauseButton";
import { COLORS, DEFAULT_MARGIN } from "../constants";
import { t } from "../services/i18n";
import { streamTrack, usePlayerService } from "../services/player";
import textStyles from "../styles/text";
import AnimatedLiveLight from "./AnimatedLiveLight";

const LiveStream = () => {
    const [playing, setPlaying] = useState(false);
    const isDarkMode = useColorScheme() === 'dark';

    const playerService = usePlayerService()

    playerService.addPlayPauseListener((playing : boolean) => {
        setPlaying(playing && playerService.track == streamTrack );
      }
    );

    playerService.addTrackChangeListener((current) => {
        if(playerService.track != streamTrack) setPlaying(false);
    });

    const PlayPauseButton = playing ? PauseButton : PlayButton;

    const width  = Dimensions.get("window").width;

    const onPress = () => {
        if(playerService.track != streamTrack) {
            playerService.loadTrack(streamTrack)
            setPlaying(true);
        } else {
            setPlaying(!playing);
            playerService.togglePlaying();
        }
    };

    const height = Math.floor(width /  2.0);

    const imageSource = require("../assets/map.png");

    const lineHeight = textStyles(true).lineHeight;

    const wrapperStyles = {
        flex: 1,
        width: width,
        height: height
    }

    const iconStyles = {
        marginLeft: DEFAULT_MARGIN / 2,
        marginTop: DEFAULT_MARGIN  / 2,
        width: width / 6,
        height: width / 6
    }

    return (
        <TouchableOpacity style={wrapperStyles} onPress={onPress} activeOpacity={1}>
            <View style={{
            flexDirection: "row",
            flex: 0.16,
            backgroundColor: COLORS.black,
            paddingLeft: DEFAULT_MARGIN / 2,
            paddingRight: DEFAULT_MARGIN / 2
        }}>
                <AnimatedLiveLight  style={{height: lineHeight, width: lineHeight, marginTop: lineHeight /3, marginRight: lineHeight/3}}  />

                <Text style={{fontWeight: "bold", marginRight: 0, marginTop: lineHeight / 3.5, ...textStyles(true)}}>{` ${t("liveButtonLabel").toUpperCase()}`}</Text>
            </View>
            <ImageBackground source={imageSource} style={{ flex: 0.84 }}>
                <View style={iconStyles}>
                    <PlayPauseButton />
                </View>
            </ImageBackground>
        </TouchableOpacity>
    );
}

export default LiveStream