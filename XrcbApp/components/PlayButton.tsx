import React from "react";
import { Image, useColorScheme } from "react-native";

const PlayButton = () => {
    const isDarkMode = useColorScheme() === 'dark';
    const darkPlayButton = require("../assets/player/darkPlayButton.png");
    const lightPlayButton = require("../assets/player/lightPlayButton.png");
    return (
        <Image source={isDarkMode ? darkPlayButton : lightPlayButton} style={{height: "100%", aspectRatio: 1.0, flex: 1}} />
    );
}

export default PlayButton