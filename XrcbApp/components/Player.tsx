import React, { useState } from 'react';
import { View, TouchableOpacity, Easing, useColorScheme, Image, Text } from 'react-native';
import TextTicker from "react-native-text-ticker";
import  { usePlayerService, Track } from "../services/player";
import { LABEL_LENGTH, LABEL_SPACING, COLORS } from "../constants"
import PlayButton from "./PlayButton";
import PauseButton from "./PauseButton";
import ForwardButton from "./ForwardButton";
import BackButton from "./BackButton";
import textStyles from "../styles/text";
import liveLight from "../assets/player/liveLight.png";


type PropsType = { style?: object }

const Player = ({  style={} } : PropsType) => {
  const playerService = usePlayerService();
  const [playing, setPlaying] = useState(false);
  const [current, setCurrent] = useState(playerService.getNowPlaying());
  const [isLive, setIsLive] = useState(playerService.track?.live)
  const isDarkMode = useColorScheme() === 'dark'

  playerService.addPlayPauseListener((playing : boolean) => {
      setPlaying(playing);
    }
  );

  playerService.addTrackChangeListener((current) => {
      setCurrent(current);
      setIsLive(playerService.track?.live)
  });

  const repetitions = Math.max(1, Math.floor(LABEL_LENGTH/(current.length + LABEL_SPACING)));
  const currentNormalisedLength = (current + " ".repeat(LABEL_SPACING)).repeat(repetitions).padEnd(LABEL_LENGTH);



  const styles = {...{
    backgroundColor: (isLive ? COLORS.pink : COLORS.yellow),
    flex: 0.15,
    flexDirection: "row",
    justifyContent: 'space-between',
    alignItems: 'center'
  }, ...style}



  const lightStyle = {
    height: textStyles(isDarkMode).fontSize,
    width: textStyles(isDarkMode).fontSize,
    marginTop: 12,
    marginRight: 10
  }

  const labelStyle = {
    ...textStyles(isDarkMode),
    ... { flexGrow: 1, marginTop: 8 }
  }

  const PlayPauseButton = playing ? PauseButton : PlayButton;

  const onPressPlayPause = () => {
    setPlaying(!playing) // this gets set in the listener but we do it first for responsiveness.
    playerService.togglePlaying()
  }

  const onPressForward = () => { playerService.fastForward(); };

  const onPressBack = () => { playerService.rewind() };

  return(
    <View style={styles}>
    <View style={{flex: isLive ? 0.1 : 0.3, aspectRatio: isLive ? 1.0 : 3.0, marginRight: 5, flexDirection: "row"}}>
        <TouchableOpacity onPress={onPressPlayPause} activeOpacity={1} style={{flex: isLive ? 1 : 0.31, paddingTop: 5, paddingBottom: 5}}>
          <PlayPauseButton />
        </TouchableOpacity>
        { !isLive &&
              <TouchableOpacity onPress={onPressBack} activeOpacity={1} style={{flex: 0.345, paddingTop: 5, paddingBottom: 5}}>
                <BackButton />
              </TouchableOpacity>
        }
        {
          !isLive &&
              <TouchableOpacity onPress={onPressForward} activeOpacity={1} style={{flex: 0.345,  paddingTop: 5, paddingBottom: 5}}>
                <ForwardButton />
              </TouchableOpacity>
        }

    </View>
    <View style={{flex: isLive ? 0.9 : 0.7}}>
      <View style={{flex: 1, flexDirection: "row"}}>
        { isLive &&
            <Image style={lightStyle} source={liveLight} />
        }
        <TextTicker style={labelStyle} bounce={false} repeatSpacer={LABEL_SPACING} easing={Easing.linear} scrollSpeed={25}>
          {currentNormalisedLength}
        </TextTicker>
      </View>
    </View>
    </View>
    );
  }

  export default Player