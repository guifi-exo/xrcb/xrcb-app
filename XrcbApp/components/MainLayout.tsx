import React, { useState } from 'react';
import {
  SafeAreaView,
  useColorScheme,
  View,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { COLORS } from "../constants";
import FloatingPlayer from "./FloatingPlayer";
import LiveScreen from "../screens/Live";
import PodcastsScreen from "../screens/Podcasts";
import RadiosScreen from "../screens/Radios";
import { t } from "../services/i18n";
import { usePlayerService } from "../services/player";

import Icon from 'react-native-ionicons';

const Tab = createBottomTabNavigator();

const MainLayout = () => {
    const isDarkMode = useColorScheme() === 'dark';
    const [playerVisible, setPlayerVisible] = useState(false);
    const playerService = usePlayerService();
    playerService.addToggleVisibilityListener(setPlayerVisible);
    const backgroundStyle = {
      backgroundColor: isDarkMode ? COLORS.black : COLORS.white,
      flex: 1,
    };
    return (
        <NavigationContainer>
            <SafeAreaView style={backgroundStyle}>
                <View style={{flex: 1, flexDirection: "column"}}>
                        <Tab.Navigator style={{ flexGrow: 1 }} screenOptions={{
                            headerShown: false,
                            tabBarStyle: { backgroundColor: backgroundStyle.backgroundColor }
                        }}>
                        <Tab.Screen
                            name="Live"
                            key="Live"
                            children={()=><LiveScreen playerVisible={playerVisible} />}
                            options = {{
                                title: t("liveScreenName"),
                                tabBarIcon: ({ color, size }) => (
                                    <Icon name="wifi" color={color} size={size} />
                                )
                            }}
                        />

                        <Tab.Screen
                            name="Podcasts"
                            key="Podcasts"
                            children={()=><PodcastsScreen playerVisible={playerVisible} />}
                            options = {{
                                title: t("podcastsScreenName"),
                                tabBarIcon: ({ color, size }) => (
                                    <Icon name="recording" color={color} size={size} />
                                )
                            }}
                        />

                        <Tab.Screen
                            name="Radios"
                            key="Radios"
                            children={()=><RadiosScreen playerVisible={playerVisible}  />}
                            options = {{
                                title: t("radiosScreenName"),
                                tabBarIcon: ({ color, size }) => (
                                    <Icon name="radio" color={color} size={size} />
                                )
                            }}
                        />
                    </Tab.Navigator>
                </View>
                <FloatingPlayer />
            </SafeAreaView>
        </NavigationContainer>
    );
}

export default MainLayout;