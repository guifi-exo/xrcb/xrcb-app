import React from 'react';
import { View, Text, useColorScheme, Dimensions, ImageBackground } from 'react-native';
import { Radio } from "../models/radio";
import { DEFAULT_MARGIN, COLORS } from '../constants';
import textStyle from "../styles/text";
import { locale } from "../services/i18n";
import { truncateString } from '../utils';

const RadioListing = ({ radio } : { radio : Radio}) => {
    const imageSource = radio.imageURL ? { uri: radio.imageURL } : require("../assets/default_radio.png");
    const isDarkMode = useColorScheme() === 'dark';
    const width = Dimensions.get("window").width;
    const wrapperStyles = {
        flex: 1,
        borderTopWidth: 3,
        borderTopColor: isDarkMode ? COLORS.white : COLORS.black,
        backgroundColor: isDarkMode ? COLORS.black : COLORS.white,
    }
    const headerStyle = {
        ...textStyle(isDarkMode),
        ...{
            backgroundColor: isDarkMode ? COLORS.black : COLORS.white,
            paddingLeft: DEFAULT_MARGIN,
            paddingRight: DEFAULT_MARGIN,
            paddingTop: DEFAULT_MARGIN,
            paddingBottom: DEFAULT_MARGIN / 4,
            width: "auto",
            fontSize: 24,
            fontWeight: "bold"
        }
    }

    const titleWrapperStyle = {
        position: "absolute",
        bottom: 0
    }
    const metaWrapperStyle = {
        position: "absolute",
        top: 0,
        right: 0
    }
    const metaStyle = {
        ...textStyle(isDarkMode),
        ...{
            backgroundColor: isDarkMode ? COLORS.black : COLORS.white,
            paddingLeft: DEFAULT_MARGIN / 2,
            paddingRight: DEFAULT_MARGIN / 2,
            paddingTop: DEFAULT_MARGIN / 4,
            paddingBottom: DEFAULT_MARGIN / 4,
            width: "auto",
            fontWeight: "bold",
        }
    }

    const blurbWrapperStyle = {
        ...textStyle(isDarkMode),
        ...{
            paddingLeft: DEFAULT_MARGIN,
            paddingRight: DEFAULT_MARGIN,
            paddingTop: DEFAULT_MARGIN,
            paddingBottom: DEFAULT_MARGIN
        }
    }

    return (
        <View style={wrapperStyles} key={`radio=${radio.id}`}>
            <ImageBackground source={imageSource}  style={{width: width, height: width*2/3}}>
            <View style={metaWrapperStyle}>
                <Text style={metaStyle}>
                    {radio.metaText(locale())}
                </Text>
            </View>

            <View style={titleWrapperStyle}>
                <Text style={headerStyle}>{radio.title}</Text>
            </View>
            </ImageBackground>
            { radio.showBlurb(locale()) &&
              <View style={blurbWrapperStyle}>
                <Text>{truncateString(radio.blurb(locale()), 150)}</Text>
            </View>
            }

        </View>
    );
}

export default RadioListing