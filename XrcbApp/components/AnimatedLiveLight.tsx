import React, { useRef, useEffect } from "react";
import { Animated, Easing } from "react-native";

export default ({style} : {style : object}) => {
    const liveLight = require("../assets/player/liveLight.png");
    const fadeAnim = useRef(new Animated.Value(0)).current
    useEffect(() => {
        Animated.loop(
            Animated.sequence([
                Animated.timing(
                    fadeAnim,
                    {
                        toValue: 1,
                        duration: 250,
                        easing: Easing.quad,
                        useNativeDriver: false
                    }
                ),
                Animated.timing(
                    fadeAnim,
                    {
                        toValue: 1,
                        duration: 500,
                        useNativeDriver: false
                    }
                ),
                Animated.timing(
                    fadeAnim,
                    {
                        toValue: 0,
                        duration: 2000,
                        easing: Easing.quad,
                        useNativeDriver: false
                    }
                )
            ]),
        ).start();
      }, [fadeAnim]);
    return (
        <Animated.Image source={liveLight} style={{opacity: fadeAnim, ...style}} />
    );
}