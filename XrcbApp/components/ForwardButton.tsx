import React from "react";
import { Image, useColorScheme } from "react-native";

const ForwardButton = () => {
    const isDarkMode = useColorScheme() === 'dark';
    const darkForwardButton = require("../assets/player/darkForwardButton.png");
    const lightForwardButton = require("../assets/player/lightForwardButton.png");
    return (
        <Image source={isDarkMode ? darkForwardButton : lightForwardButton} style={{height: "100%", flex: 1, aspectRatio: 1.12}} />
    );
}

export default ForwardButton