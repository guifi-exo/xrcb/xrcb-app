import React from "react";
import { Image, useColorScheme } from "react-native";

const PauseButton = () => {
    const isDarkMode = useColorScheme() === 'dark';
    const darkPauseButton = require("../assets/player/darkPauseButton.png");
    const lightPauseButton = require("../assets/player/lightPauseButton.png");
    return (
        <Image source={isDarkMode ? darkPauseButton : lightPauseButton} style={{height: "100%", aspectRatio: 1.0, flex: 1}} />
    );
}

export default PauseButton;
