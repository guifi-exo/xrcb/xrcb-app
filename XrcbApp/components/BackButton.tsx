import React from "react";
import { Image, useColorScheme } from "react-native";

const BackButton = () => {
    const isDarkMode = useColorScheme() === 'dark';
    const darkBackButton = require("../assets/player/darkBackButton.png");
    const lightBackButton = require("../assets/player/lightBackButton.png");
    return (
        <Image source={isDarkMode ? darkBackButton : lightBackButton} style={{height: "100%", flex: 1, aspectRatio: 1.12 }} />
    );
}

export default BackButton