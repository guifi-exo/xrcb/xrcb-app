import React from 'react';
import { Text, useColorScheme } from 'react-native';

import textStyle from "../styles/text";
import { t } from "../services/i18n";
import { DEFAULT_MARGIN } from "../constants";


const Blurb = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const allStyles = {...textStyle(isDarkMode), ...{
    paddingLeft : DEFAULT_MARGIN / 2.0,
    paddingRight : DEFAULT_MARGIN / 2.0,
    marginBottom: DEFAULT_MARGIN
  }}

  return (
    <Text style={allStyles}>{t("introBlurb")}</Text>
  )
}

export default Blurb;
