import React, { useEffect, useState } from 'react';
import {View, Text, useColorScheme } from 'react-native';

import LiveEventsService from "../services/liveEvents";
import { LiveEvent } from "../models/liveEvent";
import LiveEventComponent from "./LiveEvent";
import { COLORS,  DEFAULT_MARGIN } from '../constants';
import textStyle from "../styles/text";
import PodcastListing from './PodcastListing';
import { usePodcastsService } from '../services/podcasts';
import { t, locale } from "../services/i18n";
const LiveEvents = () => {
    const podcastsService = usePodcastsService();
    const latestPodcast = podcastsService.latest();
    const [eventsService, setEventsService] = useState(null as LiveEventsService | null);

    useEffect(() => {
        const f = async () => {
            const events = await LiveEventsService.setup();
            setEventsService(events);
        };
        f();
    }, []);

    const isDarkMode = useColorScheme() === 'dark';
    const headingStyles = {...textStyle(isDarkMode), ...{
        fontSize: 24,
        lineHeight: 36,
        fontWeight: "bold",
        paddingLeft : DEFAULT_MARGIN / 2.0,
        paddingRight : DEFAULT_MARGIN / 2.0,
        marginBottom: DEFAULT_MARGIN / 2.0,
    }}

    const wrapperStyles = {
        marginBottom: DEFAULT_MARGIN * 4,
        flex: 1,
    }

    function renderPodcasts() {
        return (
            <View style={wrapperStyles}>
                <Text style={headingStyles}>{t("latestPodcastHeading")}</Text>
                <PodcastListing podcast={latestPodcast} style={{ paddingLeft: DEFAULT_MARGIN / 2, paddingRight: DEFAULT_MARGIN / 2 }} />
            </View>
        )
    }

    function renderEvents(events : Array<LiveEvent>) {
        return (
            <View style={wrapperStyles}>
                { events.map((event) => (
                    <LiveEventComponent event={event} key={`live-event-${event.id}`}/>
                ))}
            </View>
        )
    }

    if(eventsService && eventsService.upcoming().length > 0) {
        return renderEvents([eventsService.next()]);
    } else {
        return renderPodcasts();
    }
}


export default LiveEvents