import React from 'react';
import { View } from 'react-native';
import { Radio } from "../models/radio";
import PodcastListing from './PodcastListing';
import { usePodcastsService } from '../services/podcasts';
import { DEFAULT_MARGIN } from '../constants';

type PropsType = {
    radio: Radio,
};


const RadioPodcastList = ({ radio } : PropsType) => {
    const podcastsService = usePodcastsService();
    const displayedPodcasts = podcastsService.forRadio(radio);

    return (
        <View>
            {displayedPodcasts.map((podcast) => (
                <PodcastListing
                    key={podcast.id}
                    podcast={podcast}
                    style={{
                        paddingLeft: DEFAULT_MARGIN / 2,
                        paddingRight: DEFAULT_MARGIN / 2,
                    }}
                />
            ))}
        </View>
    );
}

export default RadioPodcastList;