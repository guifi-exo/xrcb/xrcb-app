import React from 'react';
import { Dimensions, Image, View, useColorScheme } from 'react-native';

import { DEFAULT_MARGIN } from '../constants';

const Logo = () => {
  const width = Dimensions.get("window").width - DEFAULT_MARGIN * 2; // TODO HACK - is it really not possible to get the
                                                                     // rendered pixel width of the image?
  const isDarkMode = useColorScheme() === 'dark';
  const darkLogo = require("../assets/logos/logo-horizontal-w-on-t.png");
  const lightLogo = require("../assets/logos/logo-horizontal-b-on-t.png");
  const padding = DEFAULT_MARGIN / 2.0;
  const logoAspectRatio = 0.477; // TODO can we not hard code this?
  return (
    <View style={{ marginBottom: DEFAULT_MARGIN, paddingLeft: padding, paddingRight: padding }}>
      <Image source={isDarkMode ? darkLogo : lightLogo} style={{width: width * 0.66, height: width * 0.66 * logoAspectRatio}} />
    </View>
  )
}

export default Logo;
