import { da } from "date-fns/locale";
import { parseEntities } from "parse-entities";
import { Locale, LocalizedStrings } from "../services/i18n";
import { replaceBreaks, normalise } from "../utils";


function cleanHtml(html : string) {
    return replaceBreaks(parseEntities(html)).replace(/<[^>]+>/g, '');
}

export type RadioJson = {
    "ID": number,
    "post_title": string,
    "post_date": Date,
    "link": string,
    "rss_feed": string,
    "acf": {
        "barrio": string,
        "sede": string,
        "anyo_fundacion": string,
        "historia": string
        "historia_es": string,
        "historia_en": string,
        "licencia": string,
        "web": string,
        "fm": string,
        "img_podcast": {
            "sizes": {
                "large": string,
            }
        } | undefined,
    }
}

export class Radio {
    title : string;
    id : number;
    imageURL : string | undefined;
    barrio : string;
    year : string;
    licence : string;
    freq : string;
    link : string;
    rss_feed : string;
    blurbs : LocalizedStrings;

    constructor(data: RadioJson) {
        this.id = data.ID;
        this.title = parseEntities(data.post_title);
        this.imageURL = data.acf.img_podcast?.sizes?.large;
        this.barrio = data.acf.barrio;
        this.year = data.acf.anyo_fundacion;
        this.licence = data.acf.licencia;
        this.freq = data.acf.fm;
        this.link = data.link;
        this.rss_feed = data.rss_feed;
        this.blurbs = {
            en: cleanHtml(data.acf.historia_en),
            es: cleanHtml(data.acf.historia_es),
            ca: cleanHtml(data.acf.historia)
        }
    }

    metaText(locale="ca") {
        return [
            this.barrio,
            this.year,
            this.licenceFormatted(),
            this.freqFormatted(),
        ].join(" / ")
    }

    licenceFormatted() {
        return this.licence == "CR" ? "©" : this.licence;
    }

    freqFormatted() {
        return this.freq ? (this.freq + "FM") : "www";
    }

    blurb(locale : Locale ="ca") {
        return this.blurbs[locale];
    }

    showBlurb(locale : Locale = "ca") {
        return this.blurbs[locale] !== undefined && this.blurbs[locale].length > 0;
    }

    normalisedText() {
        return normalise(this.title, this.barrio, this.year, this.blurbs.es, this.blurbs.ca, this.blurbs.en);
    }

    rssURL() {
        return this.rss_feed.replace(/^https?/, "feed");
    }
}