import { parse as dateParse, format as dateFormat } from 'date-fns';
import { normalise } from "../utils";
import { PodcastTrack } from "../services/player";
import { parseEntities } from "parse-entities";
import { ca, enGB, es } from 'date-fns/locale';

const locales = {
    "ca": ca,
    "en": enGB,
    "es": es
}


export type PodcastJson = {
    title: string;
    description: string;
    radio_name: string;
    file_mp3: string;
    id: number;
    radio_permalink: string;
    radio_id: number;
    fecha_publicacion: string;
    permalink: string;
};

export class Podcast {
    id: number;
    title: string;
    description: string;
    radio_name: string;
    radio_id: number;
    media_url: string;
    url: string;
    radio_url: string;
    publication_date: Date;

    constructor(data: PodcastJson) {
        this.title = parseEntities(data.title);
        this.description = parseEntities(data.description);
        this.radio_name = parseEntities(data.radio_name);
        this.id = data.id;
        this.url = data.permalink;
        this.media_url = data.file_mp3;
        this.radio_url = data.radio_permalink;
        this.radio_id = data.radio_id;
        this.publication_date = dateParse(data.fecha_publicacion, "yyyyMMdd", new Date());
    }

    normalisedText() {
        return normalise(this.title, this.radio_name, this.description);
    }

    track() {
        return new PodcastTrack(this);
    }

    formattedDate(localeName : "ca" | "en" | "es" = "ca") {
        const locale = locales[localeName];
        return dateFormat(this.publication_date ,"do MMM yyyy", { locale: locale });
    }
}
