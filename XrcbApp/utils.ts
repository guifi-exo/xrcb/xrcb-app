export function normalise(...args : string[]) {
  return args.map((arg) => {
    if(arg === undefined) {
      return "";
    } else {
      return arg.normalize("NFD").
      replace(/[\u0300-\u036f]/g, "").
      toLowerCase().
      replace(/['’]/g, " ").
      replace(/[^a-z0-9\s]+/g, "")
    }
  }).join(" ").replace(/\s+/g, " ").trim();
}

export type DictOfType<T> = { [key : string] : T }

export function makeDefaultDict<T>(defaultVal: T) : DictOfType<T>  {
  const obj : DictOfType<T> = {} ;
  return new Proxy(obj, {
    get: (target, name : string) => name in target ? target[name] : defaultVal
  })
}

export function replaceBreaks(str : string) : string {
  return str.replace(/\<br \/\>/g, "\n");
}

export function truncateString(str : string, len : number = 100, ellipsis="...") : string {
  if (str.length <= len) {
    return str;
  } else {
    return str.substring(0, len - ellipsis.length) + ellipsis;
  }
}