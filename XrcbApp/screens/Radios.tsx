import React from 'react';
import  { View, useColorScheme , Text} from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import RadiosList from "../components/RadiosList";
import screenStyle from "../styles/screen";
import RadioShow from '../components/RadioShow';
import { t } from '../services/i18n';

const Stack = createNativeStackNavigator();

type NavProps = {
    List: {}
    Show: { radioID : number }
}
export type ListNavigationProps = NativeStackScreenProps<NavProps, "List">
type ShowNavigationProps = NativeStackScreenProps<NavProps, "Show">

type PropsType = {
    playerVisible : boolean,

 }

const RadiosScreen = ({
    playerVisible
} : PropsType ) => {


    const isDarkMode = useColorScheme() === 'dark';

    const List = ({navigation} : ListNavigationProps ) => (
        <RadiosList navigation={navigation}/>
    );

    const Show = ({route} : ShowNavigationProps ) => (
        <RadioShow radioID={route.params.radioID} />
    );

    return (
        <View style={screenStyle(isDarkMode, playerVisible, false, false)}>
            <Stack.Navigator
                initialRouteName="List"
            >
                <Stack.Screen
                    name="List"
                    component={List}
                    options={{
                        title: t("radiosScreenName"),
                        headerShown: false
                    }}
                />

                <Stack.Screen
                    name="Show"
                    component={Show}
                    options={{
                        title: t("radiosScreenName")
                    }}
                />
            </Stack.Navigator>
        </View>

        )
    }

    export default RadiosScreen;