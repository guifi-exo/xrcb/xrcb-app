import React from 'react';
import { ScrollView, useColorScheme } from 'react-native';
import { streamTrack } from "../services/player";

import Logo from "../components/Logo";
import Blurb from "../components/Blurb";
import Player from "../components/Player";
import LiveEvents from "../components/LiveEvents";
import LiveStream from "../components/LiveStream";

import screenStyle from "../styles/screen";

import { DEFAULT_MARGIN } from "../constants";

type PropsType = { playerVisible: boolean };

const LiveScreen = ({ playerVisible } : PropsType) => {
  const isDarkMode = useColorScheme() === 'dark';
  const style = screenStyle(isDarkMode, playerVisible, false);
  return (
    <ScrollView style={style} contentContainerStyle={{paddingBottom: playerVisible ? 50 : 0}}>
        <Logo />
        <Blurb />
        <LiveStream />
        <LiveEvents />
    </ScrollView>
  )
}

export default LiveScreen;
