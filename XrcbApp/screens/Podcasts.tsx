import React, { useState, useEffect } from 'react';

import { View, FlatList, Text, useColorScheme } from 'react-native';

import screenStyle from "../styles/screen";
import textStyle from "../styles/text";
import { usePodcastsService } from '../services/podcasts';
import { Podcast } from "../models/podcast";

import SearchInput from "../components/SearchInput";
import PodcastListing from "../components/PodcastListing";
import { t } from '../services/i18n';
import { DEFAULT_MARGIN } from '../constants';

type PropsType = { playerVisible : boolean };

const PodcastsScreen = ({ playerVisible } : PropsType) => {
  const podcastsService = usePodcastsService();
  const isDarkMode = useColorScheme() === 'dark';
  const style = {
    ...screenStyle(isDarkMode, playerVisible, false),
    ...{
      flex: 1,
      flexGrow: 1,
      marginTop: 0,
      paddingTop: 0,
      flexDirection: "column",
    }};

    const  headingStyle = {...textStyle(isDarkMode), ...{
      marginLeft: 0,
      paddingLeft: DEFAULT_MARGIN / 2,
      paddingRight: DEFAULT_MARGIN / 2,
      marginTop: 0,
      paddingTop: 0,
      fontSize: 24,
      lineHeight: 36,
      marginBottom: DEFAULT_MARGIN/2,
      fontWeight: "bold",
    }}


    const [searchTerm, setSearchTerm] = useState(undefined as string | undefined);
    const[displayedPodcasts, setDisplayedPodcasts] = useState([] as Array<Podcast>);
    useEffect(() => {
      if(searchTerm === undefined || searchTerm == "") {
        setDisplayedPodcasts(podcastsService.recent());
      } else if (searchTerm.length < 3) {
        // Do nothing - display what's currently displayed
      } else {
        setDisplayedPodcasts(podcastsService.search(searchTerm));
      }
    }, [searchTerm]);

    const renderPodcast= ({ item } : { item : Podcast }) => {
      return (
        <PodcastListing
        key={item.id}
        podcast={item}
        searchTerm={searchTerm}
        />
        );
      };

      return (
        <View style={style}>
        <SearchInput onChange={setSearchTerm} />
        <Text style={headingStyle}>
        {
          searchTerm === undefined || searchTerm == "" ?
          t("podcastsHeadingRecent") :
          `${displayedPodcasts.length} ${ displayedPodcasts.length == 1 ? t("podcastsHeadingResultsSingular") : t("podcastsHeadingResultsPlural")}`
        }
        </Text>
        <FlatList
        data={displayedPodcasts}
        renderItem={renderPodcast}
        keyExtractor={podcast => podcast.id.toString()}
        />
        </View>
        )
      }

      export default PodcastsScreen;
