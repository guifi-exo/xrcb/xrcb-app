#!/usr/bin/env bash
export JAVA_HOME=/usr/local/java/jdk-15.0.2/
cd ./android
./gradlew clean
./gradlew bundleRelease
java -jar  /usr/local/bundletool-all-1.17.2.jar build-apks --bundle=app/build/outputs/bundle/release/app-release.aab --output=app/build/outputs/bundle/release/app-release.apks --mode=universal --ks=app/timc-upload-key.keystore --ks-key-alias=timc-google-play-upload
cd app/build/outputs/bundle/release
unzip app-release.apks
cd ../../../../../../

