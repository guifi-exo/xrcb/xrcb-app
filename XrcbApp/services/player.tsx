import React, { createContext, useContext, useEffect } from "react";
import TrackPlayer, { State, Capability, Event, MetadataOptions } from 'react-native-track-player';
import BackgroundTimer from "react-native-background-timer";
import { parseEntities } from "parse-entities";

import { STREAM_URL, REFRESH_TIMEOUT, PLAYER_SKIP_INTERVAL } from '../constants';
import { getCurrentMetadata } from "./api";

import { Podcast } from "../models/podcast";


type Metadata = {title : string, artist : string};
type MetadataCallback = (() => Promise<Metadata | null>) | (() => Metadata | null)

export class Track {

    url : string;
    metadata : Metadata;
    updateMetadataCallback? : MetadataCallback;
    live : boolean;

    constructor(
        url : string,
        metadata : Metadata,
        live : boolean  = true,
        updateMetadataCallback? : MetadataCallback
        ) {
            this.url = url
            this.metadata = metadata
            this.live = live
            this.updateMetadataCallback = updateMetadataCallback
        }

        getNowPlaying() {
            return parseEntities(`${this.metadata.artist} – ${this.metadata.title}`);
        }

        asTrackPlayerObject() {
            return {
                url: this.url,
                artist: this.metadata.artist,
                title: this.metadata.title
            }
        }

        async updateMetadata() {
            let changed = false;
            if(this.updateMetadataCallback !== undefined) {
                const meta = await this.updateMetadataCallback()
                changed = (meta !== null && meta != this.metadata);
                if(changed && meta !== null) {
                    this.metadata = meta
                }
            }
            return {
                metadata: this.metadata,
                nowPlaying: this.getNowPlaying(),
                changed: changed
            };
        }

    }

 export class PodcastTrack extends Track {
        constructor(podcast : Podcast) {
            const meta = { title: podcast.title, artist: podcast.radio_name };
            super(podcast.media_url, meta, false, undefined);
        }
    }

const defaultStreamMeta =  {
    title: 'Reproducció en directe',
    artist: 'XRCB'
}

export const streamTrack = new Track(STREAM_URL,defaultStreamMeta, true, async () => {
    const current = await getCurrentMetadata();
    return {
        title: current?.title || defaultStreamMeta.title,
        artist: current?.artist || defaultStreamMeta.artist,
    }
});

    class PlayerService {

        private static STREAM_CAPABILITIES = [
            Capability.Play,
            Capability.Pause,
            Capability.Stop,
        ]

        private static PODCAST_CAPABILITIES = PlayerService.STREAM_CAPABILITIES.concat([
            Capability.JumpBackward,
            Capability.JumpForward,
        ]);


        private static instance: PlayerService;
        track : Track | undefined;
        private trackChangeListeners : Array<(nowPlaying : string) => void>;
        private visibilityListeners : Array<(visible : boolean) => void>;
        visible : boolean = false;

        public static getInstance() {
            if (!PlayerService.instance) {
                PlayerService.instance = new PlayerService();
            }

            return PlayerService.instance;
        }

        constructor() {
            this.trackChangeListeners = [];
            this.visibilityListeners = [];
            this.track = undefined;
        }

        async setup() {
            await TrackPlayer.setupPlayer();
            await TrackPlayer.updateOptions({
                forwardJumpInterval: PLAYER_SKIP_INTERVAL,
                backwardJumpInterval: PLAYER_SKIP_INTERVAL,
                stopWithApp: true,
                capabilities: PlayerService.STREAM_CAPABILITIES,
                compactCapabilities: PlayerService.STREAM_CAPABILITIES
            });
            await this.loadTrack(streamTrack, false);
            await this.refreshMetadata();
        }

        async destroy() {
            return TrackPlayer.destroy();
        }

        async setupWorker() {
            await this.setupRefresh();
            return this.setupListeners();
        }

        addTrackChangeListener(callback : (nowPlaying : string) => void) {
            this.trackChangeListeners.push(callback);
        }

        addToggleVisibilityListener(callback: (visible : boolean) => void) {
            this.visibilityListeners.push(callback);
        }

        async addPlayPauseListener(callback : (playing : boolean) => void) {
            return TrackPlayer.addEventListener(Event.PlaybackState, (data) => {
                if(State.None !== data.state) {
                    callback([State.Playing, State.Connecting, State.Buffering].includes(data.state));
                }
                return true;
            });
        }

        toggleVisibilityOn() {
            if(!this.visible) {
                this.visible = true;
                this.visibilityListeners.forEach((listener) => listener(this.visible));
            }
        }

        toggleVisibilityOff() {
            if(this.visible) {
                this.visible = false;
                this.visibilityListeners.forEach((listener) => listener(this.visible));
            }
        }

        async togglePlaying() {
            if(this.track !== undefined) {
                const playing = await this.isPlaying();
                if(playing) {
                    await this.pause();
                } else {
                    await this.play();
                }
            }
        }

        getNowPlaying() {
            if (this.track !== undefined) {
                return this.track.getNowPlaying();
            }  else {
                return "";
            }
        }

        async loadTrack(track : Track, startPlaying = true) {
            // I'm gonna comment this in detail because it's slightly counterintuitive, and
            // is the only way i can find to make the player behave well in the following way:
            // - Updates metadata for the live stream in the player on the fly
            // - when the live stream is paused, it picks up again at the current on-air time
            // - when a podcast is paused, it picks up again where paused
            // - the same player works for both podcasts and the live stream.
            // So first, we need to know if a track is currently playing:
            const index = await TrackPlayer.getCurrentTrack();
            // Then we assign the current playing track here in our player wrapper.
            this.track = track;
            // We add it to react-native-track-player's queue.
            await TrackPlayer.add([track.asTrackPlayerObject()]);
            // We call the track change callbacks to set the metadata
            this.trackChangeListeners.forEach((callback) => {
                callback(track.getNowPlaying());
            });
            // If this is null, there is nothing playing, and nothing in the queue. We don't need to do anything else.
            if (index !== null) {
                // However if a current track is playing, we need to skip forward to the one we just added....
                await TrackPlayer.skipToNext();
                // Then delete everything in the queue up to the *previous* currently playing position.
                // This leaves the queue with a single track (the one we just added).
                if(index !== null) {
                    for(var x = 0; x <= index; x++) {
                        await TrackPlayer.remove(x);
                    }
                }
            }
            this.toggleVisibilityOn();
            // set the appropriate capabilities on the native player for the track:
            TrackPlayer.updateOptions({
                capabilities: track.live ? PlayerService.STREAM_CAPABILITIES : PlayerService.PODCAST_CAPABILITIES,
                compactCapabilities: track.live ? PlayerService.STREAM_CAPABILITIES : PlayerService.PODCAST_CAPABILITIES
            });

            // Finally, *if* this track load was user initiated, and we're not already playing, we start the player automatically on load.
            // The startPlaying flag is needed as in the case of the live stream (a non-live track) we eagerly reload the track
            // every time it's paused, but don't restart playing. This ensures that it's not buffered, and when the user clicks play again,
            // it restarts at the on-air time, not the time they paused, and the metadata displayed stays in sync.
            const playing = await this.isPlaying();
            if(!playing && startPlaying) {
                this.play();
            }

        }

        private async isPlaying() {
            const state = await TrackPlayer.getState();
            return state == State.Playing
        }

        private async play() {
            if(this.track === undefined) {
                return false
            }
            if(this.track.live) {
                // Eagerly reload the track to prevent buffering while it's paused. This ensures it will start playing again at the current
                // on-air time.
                await this.loadTrack(this.track, false);
            }
            await TrackPlayer.play()
            await this.refreshMetadata();
            this.toggleVisibilityOn();
            return true;
        }

        private async pause() {
            if(this.track === undefined) {
                return false
            }
            await TrackPlayer.pause();
            return true;
        }

        async stop() {
            await this.pause();
            await this.clearQueue();
            await this.refreshMetadata();
            await this.destroy();
            this.toggleVisibilityOff();
            return true;
        }


        async fastForward () {
            const position = await TrackPlayer.getPosition();
            const length = await TrackPlayer.getDuration();
            if(position + PLAYER_SKIP_INTERVAL < length) {
                await TrackPlayer.seekTo(position + PLAYER_SKIP_INTERVAL);
            } else {
                await TrackPlayer.seekTo(length - 1);
            }

        }

        async rewind() {
            const position = await TrackPlayer.getPosition();
            if(position - PLAYER_SKIP_INTERVAL > 0) {
                await TrackPlayer.seekTo(position - PLAYER_SKIP_INTERVAL);
            } else {
                await TrackPlayer.seekTo(0);
            }

        }

        private async setupListeners() {
            await TrackPlayer.addEventListener(Event.RemotePlay, async () => {  return this.play(); });
            await TrackPlayer.addEventListener(Event.RemotePause, async () => { return this.pause(); });
            await TrackPlayer.addEventListener(Event.RemoteJumpForward, async () => { return this.fastForward(); });
            await TrackPlayer.addEventListener(Event.RemoteJumpBackward, async() => { return this.rewind(); });
            return TrackPlayer.addEventListener(Event.RemoteStop, async() => { return this.stop(); });
        }

        private setupRefresh() {
            BackgroundTimer.runBackgroundTimer(this.refreshMetadata , REFRESH_TIMEOUT);
        }

        private async clearQueue() {
            await TrackPlayer.reset();
            this.track = undefined;
            return true;
        }

        async refreshMetadata() {
            if(this.track) {
                const result = await this.track.updateMetadata();
                if(result?.changed) {
                    this.trackChangeListeners?.forEach((callback) => {
                        callback(result.nowPlaying);
                    });
                    const currentIndex = await TrackPlayer.getCurrentTrack();
                    return TrackPlayer.updateMetadataForTrack(currentIndex, result.metadata);
                } else {
                    return true;
                }
            } else {
                this.trackChangeListeners?.forEach((callback) => {
                    callback("");
                });
                return true;
            }
        }
    }

    const PlayerContext = createContext(undefined as PlayerService | undefined);

    const Provider = ({ children } : { children : React.ReactNode }) => {
        const playerService = PlayerService.getInstance();
        useEffect(() => {
            const f = async () => {
                await playerService.setup();
            }
            f();
        }, []);
        return (
            <PlayerContext.Provider value={playerService}>
                {children}
            </PlayerContext.Provider>
        )
    }

    const usePlayerService : () => PlayerService = () => useContext(PlayerContext)!;

    export default PlayerService;
    export { PlayerService as Service, usePlayerService, Provider };