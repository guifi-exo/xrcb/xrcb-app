import React, { createContext, useContext, useEffect, useState } from 'react';
import createTrie from "autosuggest-trie";

import { normalise, DictOfType } from "../utils";
import { getPodcasts } from './api';
import { useLoadListeners } from './appLoadListeners';
import { Podcast, PodcastJson } from '../models/podcast';
import { Radio } from "../models/radio";

function sortedByDate(podcasts: Array<Podcast>) {
    return podcasts.sort((a, b) => (b.publication_date.getTime() - a.publication_date.getTime()))
}

type TrieData = { id: number, text : string };

export default class PodcastsService {
    podcasts : Array<Podcast>;
    podcastsDict : DictOfType<Podcast>;
    podcastsTrie: { getMatches: (query: string) => Array<TrieData> };
    podcastsByRadio: DictOfType<Array<Podcast>>;

    static async setup() {
        const podcastsData = await getPodcasts();
        if(podcastsData !== null) {
            return new PodcastsService(podcastsData.reverse());
        } else {
            return undefined;
        }
    }

    constructor(podcastData: Array<PodcastJson>) {
        this.podcasts = [];
        this.podcastsDict = {}
        this.podcastsByRadio = {} //makeDefaultDict([]);
        let trieData : Array<TrieData> = [];
        podcastData.forEach((data) => {
            const podcast = new Podcast(data);
            this.podcastsDict[podcast.id] = podcast;
            trieData.unshift({id: podcast.id, text: podcast.normalisedText()})
            this.podcasts.unshift(podcast);
            if(!Object.keys(this.podcastsByRadio).includes(data.radio_id.toString())) {
                this.podcastsByRadio[data.radio_id.toString()] = [];
            }
            this.podcastsByRadio[data.radio_id.toString()].unshift(podcast);
        });
        this.podcastsTrie = createTrie(trieData, "text");
        this.podcasts = sortedByDate(this.podcasts);
    }

    search(searchTerm : string) {
        const results : Array<TrieData> = this.podcastsTrie.getMatches(normalise(searchTerm));
        return sortedByDate([... new Set(results.map((d) => this.podcastsDict[d.id]))]);
    }

    recent(page : number = 0, perPage : number = 10) {
        return this.podcasts.slice(page * perPage, page * perPage + perPage);
    }

    latest() {
        return this.podcasts[0];
    }

    forRadio(radio : Radio) {
       return sortedByDate(this.podcastsByRadio[radio.id.toString()] || []);
    }
}

const PodcastsContext = createContext(undefined as PodcastsService | undefined);

const Provider = ({children} : { children : React.ReactNode }) => {
    const loadListeners = useLoadListeners();
    const [podcastsService, setPodcastsService] = useState(undefined as PodcastsService | undefined);
    useEffect(() => {
        const f = async () => {
            const result = await PodcastsService.setup();
            if(result) {
                setPodcastsService(result);
                loadListeners.loaded("podcastsService");
            } else {
                loadListeners.failed("podcastsService");
            }
        }
        f();
    }, []);
    return (
        <PodcastsContext.Provider value={podcastsService}>
            {children}
        </PodcastsContext.Provider>
    )
};

const usePodcastsService : () => PodcastsService = () => useContext(PodcastsContext)!;
export { PodcastsService as Service, usePodcastsService, Provider}
