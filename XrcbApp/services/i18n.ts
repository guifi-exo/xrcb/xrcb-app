import * as RNLocalize from "react-native-localize";


export type Locale = "en" | "es" | "ca";
export interface LocalizedStrings {en: string, es: string, ca: string };


const strings : Record<string, Record<string, string>>= {
  en: require("../translations/en.json"),
  es: require("../translations/es.json"),
  ca: require("../translations/ca.json")
}

const DEFAULT_LOCALE : Locale = 'ca';

const locale = () : Locale => {
  const locale = RNLocalize.findBestAvailableLanguage(
    Object.keys(strings),
  )?.languageTag;
  return (locale as Locale) || DEFAULT_LOCALE;
}

const t = (key : string) : string => {

  return strings[locale()][key];
}

export { t, locale, DEFAULT_LOCALE }
