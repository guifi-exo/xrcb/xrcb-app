import React, {createContext, useContext} from "react";
import { AppState } from "react-native";

class AppLoadListeners {
    private static instance : AppLoadListeners | undefined;
    private loadStatuses : { [ index : string ] : boolean } = {};
    private awaitingLoaders : Set<string> = new Set<string>();
    private listeners : Array<(result : boolean, error?: any) => void> = [];

    public static getInstance() : AppLoadListeners {
        if(!AppLoadListeners.instance) {
            AppLoadListeners.instance = new AppLoadListeners();
        }
        return AppLoadListeners.instance;
    }

    registerLoader(name : string ) {
        this.awaitingLoaders.add(name);
    }

    reset() {
        this.loadStatuses = {};
    }

    registerListener(f : (result : boolean, error? : any) => void) {
        this.listeners.unshift(f);
    }

    loaded(name : string) {
        this.loadStatuses[name] = true;
        this.callListeners();
    }

    failed(name : string, error? : any) {
        this.loadStatuses[name] = false;
        this.callListeners(error);
    }

    callListeners(error? : any) {
        const allLoaded = [...this.awaitingLoaders].map(
            (k) => this.loadStatuses[k]
        ).every((e) => (e === true))
        const anyFailed = [...this.awaitingLoaders].map(
            (k) => this.loadStatuses[k]
        ).some((e) => (e === false))
        if(allLoaded) {
            this.listeners.forEach((l) => l(true));
        } else if (anyFailed) {
            this.listeners.forEach((l) => l(false, error));
        }
     }
}

const Context = createContext(undefined as AppLoadListeners | undefined);

const Provider = ({children}: { children : React.ReactNode}) => {
    return (
        <Context.Provider value={AppLoadListeners.getInstance()}>
            {children}
        </Context.Provider>
    );
}

const useLoadListeners : () => AppLoadListeners = () => useContext(Context)!

export {Provider, useLoadListeners};
