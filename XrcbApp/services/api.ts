import { NOW_PLAYING_URL, PODCASTS_URL, LIVE_EVENTS_URL, RADIOS_URL } from "../constants";
import { PodcastJson } from "../models/podcast";
import { LiveEventJson } from "../models/liveEvent";
import { RadioJson } from "../models/radio";
export async function getCurrentMetadata() {
  try {
    const meta  = await fetch(NOW_PLAYING_URL);
    const metaJson = await meta.json();
    if(metaJson.data !== null) {
      return {
        "title": metaJson.data.title,
        "artist": metaJson.data.radio_name
      }
    } else {
      return null;
    }
  } catch {
    return null;
  }
}

export async function getPodcasts() : Promise<Array<PodcastJson> | null> {
  try {
    const meta  = await fetch(PODCASTS_URL);
    const metaJson = await meta.json();
    if(metaJson.data !== null) {
      return metaJson.data;
    } else {
      return null;
    }
  } catch {
    return null;
  }
}

export async function getLiveEvents() : Promise<Array<LiveEventJson> | null> {
  try {
    const meta = await fetch (LIVE_EVENTS_URL);
    const metaJson = await meta.json();
    if(metaJson.data !== null) {
      return metaJson.data;
    } else {
      return null;
    }
  } catch {
    return null;
  }
}


export async function getRadios() : Promise<Array<RadioJson> | null> {
  try {
    const meta = await fetch(RADIOS_URL);
    const metaJson = await meta.json();
    if(metaJson !== null) {
      return metaJson;
    } else {
      return null;
    }
  } catch {
    return null;
  }
}