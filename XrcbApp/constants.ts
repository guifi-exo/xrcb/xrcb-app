export const STREAM_URL = 'https://icecast.xrcb.cat/main.mp3';

export const NOW_PLAYING_URL = 'https://xrcb.cat/wp-json/xrcb/v1/broadcast';

export const PODCASTS_URL = "https://xrcb.cat/en/podcasts-json/";

export const LIVE_EVENTS_URL = "https://xrcb.cat/ca/wp-json/xrcb/v1/broadcast/liveevents";

export const RADIOS_URL = "https://xrcb.cat/wp-json/xrcb/v1/radios";

export const REFRESH_TIMEOUT = 60 * 1000;

export const DEFAULT_MARGIN = 20;

export const LABEL_LENGTH = 127;
export const LABEL_SPACING = 10;

export const PLAYER_SKIP_INTERVAL = 30;

export const COLORS = {
  black: "#000",
  white: "#fff",
  yellow: "#ffff88",
  pink: "#ff8888",
  red: "#f00"
}

export const MATOMO_URL_BASE = "https://matomo.xrcb.cat/"
export const MATOMO_TRACKER_URL = `${MATOMO_URL_BASE}piwik.php`
export const MATOMO_SITE_ID = 1