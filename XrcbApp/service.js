module.exports = function(player) {
  return async function() {
    return player.setupWorker();
  }
}
