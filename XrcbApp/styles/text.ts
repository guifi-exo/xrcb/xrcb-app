import { COLORS } from "../constants";

export default (isDarkMode : boolean) => {
  return {
    fontFamily: 'Helvetica Neue',
    fontSize: 12,
    lineHeight: 18,
    color: isDarkMode ? COLORS.white : COLORS.black
  }
}
