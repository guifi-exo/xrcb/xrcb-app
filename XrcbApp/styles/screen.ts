import { COLORS, DEFAULT_MARGIN } from "../constants";

export default (isDarkMode : boolean, playerVisible : boolean, paddedSides : boolean = true, paddedTop: boolean = true) => {
  return {
    width: "100%",
    height: "100%",
    padding: paddedSides ? DEFAULT_MARGIN : 0,
    paddingTop: paddedTop ? DEFAULT_MARGIN : 0,
    paddingBottom: playerVisible ? 50 : 0,
    backgroundColor: isDarkMode ? COLORS.black : COLORS.white
  }
}
