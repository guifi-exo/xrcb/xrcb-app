/**
* Sample React Native App
* https://github.com/facebook/react-native
*
* Generated with the TypeScript template
* https://github.com/react-native-community/react-native-template-typescript
*
* @format
*/

import React, {  useState, useEffect } from 'react';
import { View } from 'react-native';

import SplashScreen from 'react-native-splash-screen';

import { useLoadListeners  } from './services/appLoadListeners';

import MainLayout from "./components/MainLayout";
import ErrorMessage from "./components/ErrorMessage";
import { streamTrack, usePlayerService } from './services/player';
import { useMatomo } from 'matomo-tracker-react-native';

const App = () => {
  const { trackAppStart } = useMatomo();
  const playerService = usePlayerService();
  const loadListeners = useLoadListeners();
  const [loadedSuccessfully, setLoadedSuccessfully] = useState(false);

  useEffect(() => {
    trackAppStart({});
  }, []);

  loadListeners.registerLoader("radiosService");
  loadListeners.registerLoader("podcastsService");
  loadListeners.registerListener((success : boolean, error) => {
    setLoadedSuccessfully(success);
    playerService.loadTrack(streamTrack, false);
    if(error) {
      console.log(error);
    }
    SplashScreen.hide();
  });

  return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        { loadedSuccessfully ? <MainLayout /> : <ErrorMessage /> }
      </View>
  );

};

export default App;
