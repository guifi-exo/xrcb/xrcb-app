/**
 * @format
 */
import React, {useEffect} from 'react';
import {AppRegistry} from 'react-native';
import TrackPlayer from 'react-native-track-player';
import MatomoTracker, {
  MatomoProvider,
} from 'matomo-tracker-react-native';

import App from './App';
import {name as appName} from './app.json';
import service from './service';
import {
  Service as PlayerService,
  Provider as PlayerProvider,
} from './services/player';
import {Provider as LoadListenersProvider} from './services/appLoadListeners';
import {Provider as PodcastsProvider} from './services/podcasts';
import {Provider as RadiosProvider} from './services/radios';
import { MATOMO_URL_BASE, MATOMO_SITE_ID, MATOMO_TRACKER_URL } from "./constants";


const playerService = PlayerService.getInstance();

const AppWrapper = () => {

  const matomo = new MatomoTracker({
    urlBase: MATOMO_URL_BASE,
    siteId: MATOMO_SITE_ID,
    trackerUrl: MATOMO_TRACKER_URL
  })

  useEffect(() => {
    // This is run when the component finally unmounts.
    return () => {
      playerService.destroy();
      AppLoadListeners.getInstance().reset();
    };
  }, []);
  return (
    <MatomoProvider instance={matomo}>
      <LoadListenersProvider>
        <PodcastsProvider>
          <RadiosProvider>
            <PlayerProvider>
              <App />
            </PlayerProvider>
          </RadiosProvider>
        </PodcastsProvider>
      </LoadListenersProvider>
    </MatomoProvider>
  );
};

AppRegistry.registerComponent(appName, () => AppWrapper);
TrackPlayer.registerPlaybackService(() => service(playerService));
