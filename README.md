
# XRCB app:
This is the [XRCB](https://xrcb.cat) app that allows to listen to live streaming and to the podcasts stored in the network of the XRCB.

## Description:
The xrcb-app is a mobile solution for the xrcb.cat web page that allows to listen to ist content. Users can navigate and search podcasts and radio stations of the network or listen to live streams.

XRCB is a non-commercial project by [Cultura Viva](https://ajuntament.barcelona.cat/culturaviva/), a Barcelona City Council project, in collaboration with the [guifi.net](https://guifi.net/)/[exo.cat](https://exo.cat) communities.

 - A map of Barcelona radio stations.
 - A channel fed by the contents of all the linked projects.
 - A research about the future of radio as a means of transmission.
 - A community

## Development

It is developed with [React](https://reactnative.dev/).

### Howto start

  - clone the repo `git clone git@gitlab.com:guifi-exo/xrcb/xrcb-app.git`
  - go to app directory `cd /path/to/xrcb-app/XrcbApp`
  - then [set up the environment](https://reactnative.dev/docs/environment-setup) install and run **npm**
    - `npm install`
    - `npm install react-native`
    - `npm install expo-cli`
    - now run `npm` with one of theese options
      - `npm run start` or `npm start`will run react-native
      - `npm run android` will run react for android emulation
      - `npm run ios` will run react for ios emulation
      - `npm run test` will test the app with `jest`
      - `npm run lin` will check and call `eslin` with this options: `. --ext .js,.jsx,.ts,.tsx`


until here you're mostly done, once you `npm start` you can code and check changes.
